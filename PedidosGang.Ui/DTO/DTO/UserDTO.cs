﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class UserDTO
    {
        private string _FullName;
        private int _CustomerId; //primary key
        private string _EmailAdress;
        private int _PhoneNumber;
        private string _City;
        private string _Country;

        public string FullName { get => _FullName; set => _FullName = value; }
        public int CustomerId { get => _CustomerId; set => _CustomerId = value; }
        public string EmailAdress { get => _EmailAdress; set => _EmailAdress = value; }
        public int PhoneNumber { get => _PhoneNumber; set => _PhoneNumber = value; }
        public string City { get => _City; set => _City = value; }
        public string Country { get => _Country; set => _Country = value; }
    }
}
