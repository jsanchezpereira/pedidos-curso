﻿using System;

namespace DTO
{
    public class OrderDTO
    {
        private int _ProductId;// primary key
        private string _Product_Type_Code;//foreign key
        private string _ProductName;
        private string _ProductPrice;
        private string _ProductDescription;
    }
}
