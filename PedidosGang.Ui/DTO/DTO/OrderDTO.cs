﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO.DTO
{
    public class OrderDTO
    {
        private int _OrderId;
        // customer id
        private string _OrderStatus;
        private DateTime _DateOrderPlaced;
        private string _OrderDetails;

        public int OrderId { get => _OrderId; set => _OrderId = value; }
        public string OrderStatus { get => _OrderStatus; set => _OrderStatus = value; }
        public DateTime DateOrderPlaced { get => _DateOrderPlaced; set => _DateOrderPlaced = value; }
        public string OrderDetails { get => _OrderDetails; set => _OrderDetails = value; }
    }
}
